# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = Hash.new
  str.split.each do |word|
    words[word] = word.length
  end
  words
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result.
# march = {rubies: 10, emeralds: 14, diamonds: 2}
# april = {emeralds: 27, moonstones: 5}
 # update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
 # moonstones: 5}
def update_inventory(older, newer)
  # turn newer to array,
  # check for each of the newer values, and update accordingly
  newer_arr = newer.to_a
  newer_arr.each do |item|
    older[item[0]] = item[1]
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_arr = word.chars
  word_hash = Hash.new(0)
  word_arr.each do |char|
    word_hash[char.downcase] += 1
  end
  word_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hsh = Hash.new(0)
  unique_arr = []
  arr.each do |el|
    hsh[el] += 1
  end
  hsh.to_a.each do |item|
    unique_arr << item[0]
  end
  unique_arr
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  e_o = Hash.new(0)
  numbers.each do |n|
    if n.even?
      e_o[:even] += 1
    else
      e_o[:odd] += 1
    end
  end
  e_o
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  # create hash with these vowels as keys
  # go through array and increment accordingly
  vowels = "aeiou"
  vowels_hash = Hash.new(0)
  vowels_arr = string.chars.select {|char| vowels.include?(char.downcase)}
  vowels_arr.each { |vowel| vowels_hash[vowel] += 1}
  vowels_hash.sort_by {|k, v| [-1 * v, k]}.to_a[0][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  result = []
  names = students.select {|k, v| v > 7}.map {|k,v| k}
  names.each do |name, idx|
    s_name = name
    names.each do |nam|
      if s_name != nam
        result << [s_name, nam]
      end
    end
  end
  new = []
  result.each do |el|
    new << el if !new.include?([el[1], el[0]])
  end
  new
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  spec_hash = Hash.new(0)
  specimens.each do |el|
    spec_hash[el] += 1
  end
  count = spec_hash.count
  max = spec_hash.values.max
  min = spec_hash.values.min
  return count ** 2 * min / max
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  vandalized_sign = vandalized_sign.delete!("!?.,:;$% ")
  return false if vandalized_sign == nil
  van_sign = vandalized_sign.downcase 
  sign_hash = Hash.new(0)
  normal_sign.chars.each do |ch|
    sign_hash[ch.downcase] += 1
  end

  van_sign.chars.each do |letter|
    if sign_hash[letter] > 0
      sign_hash[letter]-=1
    else
      return false
    end
  end
  return true
end

def character_count(str)
end
